/*** CONFIG ***/
// IRC server to connect:
var server = "irc.freenode.net";
var port = 6667;

// Channels to join
var channels = ['#3lab'];

// Nick, et caetera.
var nickname="ThreeBotJS";
var username="3lab";
var realname="0.1 + 0.2 = 0.30000000000000004";

// Misc.
var debug = false;




/*** CODE ***/
// Creating ircbot
var irc=require('irc');
var client=new irc.Client(server, nickname, {
  userName: username,
  realName: realname,
  channels: channels,
  port: port,
  stripColors: true,
  floodProtection: true
});
var topics={};
client.on('topic', function(channel, topic, nick, message) {
  topics[channel]=topic;
});

client.addListener('error', function(message) {
      console.log('error: ', message);
});

// Starting server
function startServer() {
var io=require('socket.io').listen(9001);
io.sockets.on('connection', function(socket) {
  socket.on('hello', function(data) { console.log("hello"); socket.emit("channels", { channels: channels }) });
  var address = socket.handshake.address;
  for(var i=0; i<channels.length; i++) {
    //client.say(channels[i],"Client connected: " + address.address);
  }
  // --- Registering listeners
  // - client
  client.on('join', function(channel, nick, message) { socket.emit('join', { channel: channel, nick: nick, message: message }) });
  client.on('topic', function(channel, topic, nick, message) { socket.emit('topic', { channel: channel, topic: topic, nick: nick, message: message }) });
  client.on('part', function(channel, nick, reason, message) { socket.emit('part', { channel: channel, nick: nick, reason: reason, message: message }) });
  client.on('quit', function(nick, reason, channels, message) { socket.emit('quit', { channels: channels, nick: nick, reason: reason, message: message }) });
  client.on('kick', function(channel, nick, by, reason, message) { socket.emit('kick', { channel: channel, nick: nick, by: by, reason: reason, message: message }) });
  client.on('message#', function(nick, to, text, message) { socket.emit('message', { nick: nick, to: to, text: text, message: message }) });
  client.on('names', function(channel, nicks) { socket.emit('names', {channel: channel, nicks: nicks}) });
  client.on('nick', function(oldnick, newnick, channels, message) { socket.emit('nick', {oldnick:oldnick, newnick:newnick, channels:channels, message:message});});
  // - socket
  socket.on('names', function(data) { client.send("NAMES", data['channel'] )});
  socket.on('topic', function(data) { socket.emit("topic-req", { topic: topics[data['channel']] }) });
  socket.on('channels', function(data) { socket.emit("channels", { channels: channels }) });
});
}

client.once("join",startServer);

